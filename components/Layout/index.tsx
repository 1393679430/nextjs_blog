import type { NextPage } from 'next'
import Footer from '@/components/Footer'
import Navbar from '@/components/Navbar'

const Layout: NextPage = ({ children }) => {
  return (
    <>
      <Navbar />
      <main>{children}</main>
      <Footer />
    </>
  )
}

export default Layout
