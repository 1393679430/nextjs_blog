import type { NextPage } from 'next'
import Link from 'next/link'
import { navs } from './config'
import styles from './index.module.scss'

const Navbar: NextPage = () => {
  return (
    <div className={styles.navbar}>
      <section className={styles.logo}>Blog</section>
      <section className={styles.link}>
        {navs.map((nav) => {
          return (
            <Link key={nav.label} href={nav.value}>
              <a>{nav.label}</a>
            </Link>
          )
        })}
      </section>
    </div>
  )
}

export default Navbar
